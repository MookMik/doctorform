class Form < ApplicationRecord
    belongs_to :user
    has_many :shares
    has_many :form_items
    has_many :user_forms
end
