class User < ApplicationRecord
    belongs_to :role
    has_many :shares
    has_many :forms
    has_many :user_forms
    has_many :sessions
end
