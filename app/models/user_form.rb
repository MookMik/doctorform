class UserForm < ApplicationRecord
    belongs_to :user
    belongs_to :form
    has_many :user_form_items
end
