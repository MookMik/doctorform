class ShareRole < ApplicationRecord
    has_many :shares
    has_one :share_role_permission
end
