class UserFormItem < ApplicationRecord
    belongs_to :form_item
    belongs_to :user_form
    belongs_to :dropdowns
end
