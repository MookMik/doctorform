class Share < ApplicationRecord
    belongs_to :share_role
    belongs_to :user
    belongs_to :form
end
