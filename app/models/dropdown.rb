class Dropdown < ApplicationRecord
    belongs_to :form_item
    has_many :user_form_items
end
