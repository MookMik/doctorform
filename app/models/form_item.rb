class FormItem < ApplicationRecord
    belongs_to :form
    belongs_to :data_type
    has_many :dropdowns
    has_many :user_form_items
end
