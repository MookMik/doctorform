class UserFormsController < ApplicationController
  before_action :set_user_form, only: [:show, :update, :destroy]

  # GET /user_forms
  def index
    @user_forms = UserForm.all

    render json: @user_forms
  end

  # GET /user_forms/1
  def show
    render json: @user_form
  end

  # POST /user_forms
  def create
    @user_form = UserForm.new(user_form_params)

    if @user_form.save
      render json: @user_form, status: :created, location: @user_form
    else
      render json: @user_form.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /user_forms/1
  def update
    if @user_form.update(user_form_params)
      render json: @user_form
    else
      render json: @user_form.errors, status: :unprocessable_entity
    end
  end

  # DELETE /user_forms/1
  def destroy
    @user_form.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_form
      @user_form = UserForm.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def user_form_params
      params.require(:user_form).permit(:user_id, :form_id)
    end
end
