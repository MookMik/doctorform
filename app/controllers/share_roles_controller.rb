class ShareRolesController < ApplicationController
  before_action :set_share_role, only: [:show, :update, :destroy]

  # GET /share_roles
  def index
    @share_roles = ShareRole.all

    render json: @share_roles
  end

  # GET /share_roles/1
  def show
    render json: @share_role
  end

  # POST /share_roles
  def create
    @share_role = ShareRole.new(share_role_params)

    if @share_role.save
      render json: @share_role, status: :created, location: @share_role
    else
      render json: @share_role.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /share_roles/1
  def update
    if @share_role.update(share_role_params)
      render json: @share_role
    else
      render json: @share_role.errors, status: :unprocessable_entity
    end
  end

  # DELETE /share_roles/1
  def destroy
    @share_role.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_share_role
      @share_role = ShareRole.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def share_role_params
      params.require(:share_role).permit(:role)
    end
end
