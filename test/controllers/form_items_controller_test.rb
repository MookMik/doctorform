require 'test_helper'

class FormItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @form_item = form_items(:one)
  end

  test "should get index" do
    get form_items_url, as: :json
    assert_response :success
  end

  test "should create form_item" do
    assert_difference('FormItem.count') do
      post form_items_url, params: { form_item: { av_search: @form_item.av_search, data_type_id: @form_item.data_type_id, description: @form_item.description, form_id: @form_item.form_id, nm_search: @form_item.nm_search, required: @form_item.required, title: @form_item.title } }, as: :json
    end

    assert_response 201
  end

  test "should show form_item" do
    get form_item_url(@form_item), as: :json
    assert_response :success
  end

  test "should update form_item" do
    patch form_item_url(@form_item), params: { form_item: { av_search: @form_item.av_search, data_type_id: @form_item.data_type_id, description: @form_item.description, form_id: @form_item.form_id, nm_search: @form_item.nm_search, required: @form_item.required, title: @form_item.title } }, as: :json
    assert_response 200
  end

  test "should destroy form_item" do
    assert_difference('FormItem.count', -1) do
      delete form_item_url(@form_item), as: :json
    end

    assert_response 204
  end
end
