require 'test_helper'

class DropdownsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @dropdown = dropdowns(:one)
  end

  test "should get index" do
    get dropdowns_url, as: :json
    assert_response :success
  end

  test "should create dropdown" do
    assert_difference('Dropdown.count') do
      post dropdowns_url, params: { dropdown: { dropdown_value: @dropdown.dropdown_value, form_item_id: @dropdown.form_item_id } }, as: :json
    end

    assert_response 201
  end

  test "should show dropdown" do
    get dropdown_url(@dropdown), as: :json
    assert_response :success
  end

  test "should update dropdown" do
    patch dropdown_url(@dropdown), params: { dropdown: { dropdown_value: @dropdown.dropdown_value, form_item_id: @dropdown.form_item_id } }, as: :json
    assert_response 200
  end

  test "should destroy dropdown" do
    assert_difference('Dropdown.count', -1) do
      delete dropdown_url(@dropdown), as: :json
    end

    assert_response 204
  end
end
