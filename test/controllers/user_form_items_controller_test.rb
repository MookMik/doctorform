require 'test_helper'

class UserFormItemsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @user_form_item = user_form_items(:one)
  end

  test "should get index" do
    get user_form_items_url, as: :json
    assert_response :success
  end

  test "should create user_form_item" do
    assert_difference('UserFormItem.count') do
      post user_form_items_url, params: { user_form_item: { checkbox_value: @user_form_item.checkbox_value, date_value: @user_form_item.date_value, datetime_value: @user_form_item.datetime_value, dropdown_value_id: @user_form_item.dropdown_value_id, file_value: @user_form_item.file_value, float_value: @user_form_item.float_value, form_item_id: @user_form_item.form_item_id, integer_value: @user_form_item.integer_value, string_value: @user_form_item.string_value, text_value: @user_form_item.text_value, user_form_id: @user_form_item.user_form_id } }, as: :json
    end

    assert_response 201
  end

  test "should show user_form_item" do
    get user_form_item_url(@user_form_item), as: :json
    assert_response :success
  end

  test "should update user_form_item" do
    patch user_form_item_url(@user_form_item), params: { user_form_item: { checkbox_value: @user_form_item.checkbox_value, date_value: @user_form_item.date_value, datetime_value: @user_form_item.datetime_value, dropdown_value_id: @user_form_item.dropdown_value_id, file_value: @user_form_item.file_value, float_value: @user_form_item.float_value, form_item_id: @user_form_item.form_item_id, integer_value: @user_form_item.integer_value, string_value: @user_form_item.string_value, text_value: @user_form_item.text_value, user_form_id: @user_form_item.user_form_id } }, as: :json
    assert_response 200
  end

  test "should destroy user_form_item" do
    assert_difference('UserFormItem.count', -1) do
      delete user_form_item_url(@user_form_item), as: :json
    end

    assert_response 204
  end
end
