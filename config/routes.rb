Rails.application.routes.draw do
  resources :user_form_items
  resources :user_forms
  resources :dropdowns
  resources :data_types
  resources :form_items
  resources :forms
  resources :sessions
  resources :share_role_permissions
  resources :share_roles
  resources :shares
  resources :permissions
  resources :roles
  resources :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
