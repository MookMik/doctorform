class CreateDataTypes < ActiveRecord::Migration[5.2]
  def change
    create_table :data_types do |t|
      t.string :type_value
      t.integer :ref_index

      t.timestamps
    end
  end
end
