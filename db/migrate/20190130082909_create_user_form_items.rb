class CreateUserFormItems < ActiveRecord::Migration[5.2]
  def change
    create_table :user_form_items do |t|
      t.integer :user_form_id
      t.integer :form_item_id
      t.date :date_value
      t.datetime :datetime_value
      t.string :string_value
      t.text :text_value
      t.integer :integer_value
      t.float :float_value
      t.string :checkbox_value
      t.string :file_value
      t.integer :dropdown_value_id

      t.timestamps
    end
  end
end
