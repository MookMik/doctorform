class CreateDropdowns < ActiveRecord::Migration[5.2]
  def change
    create_table :dropdowns do |t|
      t.integer :form_item_id
      t.string :dropdown_value

      t.timestamps
    end
  end
end
