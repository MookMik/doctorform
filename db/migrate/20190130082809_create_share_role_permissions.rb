class CreateShareRolePermissions < ActiveRecord::Migration[5.2]
  def change
    create_table :share_role_permissions do |t|
      t.integer :share_role_id
      t.string :permission_description

      t.timestamps
    end
  end
end
