class CreateShares < ActiveRecord::Migration[5.2]
  def change
    create_table :shares do |t|
      t.integer :user_id
      t.integer :form_id
      t.integer :share_role_id

      t.timestamps
    end
  end
end
