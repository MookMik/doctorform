class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :username, null: false, unique: true
      t.string :password
      t.string :name
      t.string :email, null: false, index: true, unique: true
      t.string :tel
      t.string :position
      t.integer :role_id, null: false, default: 2

      t.timestamps
    end
  end
end
