# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_01_30_082909) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "data_types", force: :cascade do |t|
    t.string "type_value"
    t.integer "ref_index"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "dropdowns", force: :cascade do |t|
    t.integer "form_item_id"
    t.string "dropdown_value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "form_items", force: :cascade do |t|
    t.integer "form_id"
    t.integer "data_type_id"
    t.string "title"
    t.string "description"
    t.boolean "required"
    t.boolean "nm_search"
    t.boolean "av_search"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "forms", force: :cascade do |t|
    t.integer "user_id"
    t.string "title"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", force: :cascade do |t|
    t.integer "role_id"
    t.string "permission_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.integer "user_id"
    t.string "token"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_role_permissions", force: :cascade do |t|
    t.integer "share_role_id"
    t.string "permission_description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "share_roles", force: :cascade do |t|
    t.string "role"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "shares", force: :cascade do |t|
    t.integer "user_id"
    t.integer "form_id"
    t.integer "share_role_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_form_items", force: :cascade do |t|
    t.integer "user_form_id"
    t.integer "form_item_id"
    t.date "date_value"
    t.datetime "datetime_value"
    t.string "string_value"
    t.text "text_value"
    t.integer "integer_value"
    t.float "float_value"
    t.string "checkbox_value"
    t.string "file_value"
    t.integer "dropdown_value_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_forms", force: :cascade do |t|
    t.integer "user_id"
    t.integer "form_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "username", null: false
    t.string "password"
    t.string "name"
    t.string "email", null: false
    t.string "tel"
    t.string "position"
    t.integer "role_id", default: 2, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email"
  end

end
