# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

1.times do	
	DataType.create([{
		type_value: "Date",
		ref_index: "1",
    }])
    DataType.create([{
		type_value: "Datetime",
		ref_index: "2",
    }])
    DataType.create([{
		type_value: "String",
		ref_index: "3",
    }])
    DataType.create([{
		type_value: "Text",
		ref_index: "4",
    }])
    DataType.create([{
		type_value: "Integer",
		ref_index: "5",
    }])
    DataType.create([{
		type_value: "Float",
		ref_index: "6",
    }])
    DataType.create([{
		type_value: "Checkbox",
		ref_index: "7",
    }])
    DataType.create([{
		type_value: "File",
		ref_index: "8",
    }])
    DataType.create([{
		type_value: "Dropdown",
		ref_index: "9",
		}])	
		
	ShareRole.create([{
		role: "Owner"
	}])	
	ShareRole.create([{
		role: "Other"
	}])	

	Role.create([{
		role: "Admin"
	}])	
	Role.create([{
		role: "User"
	}])	
end